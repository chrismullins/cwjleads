from django.forms import ModelForm
from .models import Lead
from django import forms
from fractions import Fraction
from camera_imagefield import CameraImageField, CameraImageWidget

class LeadForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(LeadForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })
    class Meta:
        model = Lead
        fields = ['name', 
            'work_email', 
            'phone_number', 
            'zip_code', 
            'office_size', 
            'message',
            'business_card']

class CardForm(ModelForm):
    business_card = CameraImageField(prefer_jpeg=True, default_field_name='business_card')
    class Meta:
       model = Lead
       fields = ['business_card']
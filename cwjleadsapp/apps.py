from django.apps import AppConfig


class CwjleadsappConfig(AppConfig):
    name = 'cwjleadsapp'

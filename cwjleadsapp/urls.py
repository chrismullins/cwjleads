from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('leads/', views.LeadListView.as_view(), name='dashboard'),
    path('leads/<int:pk>', views.LeadDetailView.as_view(), name='lead-detail'),
    path('submit/', views.get_lead, name='submit-lead'),
    path('cardsubmit/', views.card_lead, name='submit-card'),
    path('leads/<int:pk>/edit', views.LeadUpdate.as_view(), name='lead-update'),
    path('leads/<int:pk>/delete', views.LeadDelete.as_view(), name='lead-delete')
]

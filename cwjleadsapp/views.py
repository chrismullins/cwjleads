from django.shortcuts import render
from django.urls import reverse_lazy
from .models import Lead
from .forms import LeadForm, CardForm
from django.http import HttpResponseRedirect
from django.views import generic
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import FieldDoesNotExist
from django.views.generic.edit import UpdateView, DeleteView

# Create your views here.
def index(request):
    """
    Home page  
    """
    return render(
        request,
        'index.html',
        context={}
    )

class LeadListView(LoginRequiredMixin, generic.ListView):
    model = Lead
    
    def get_queryset(self):
        default_field = 'id'
        order_by = self.request.GET.get('order_by', default_field)
        try:
            Lead._meta.get_field(order_by)
        except FieldDoesNotExist:
            order_by = default_field
        return Lead.objects.all().order_by(order_by)

class LeadDetailView(LoginRequiredMixin, generic.DetailView):
    model = Lead

def get_lead(request):
    if request.method == 'POST':
        form = LeadForm(request.POST, request.FILES)
        if form.is_valid():
            # any processing
            form.save()
            return HttpResponseRedirect('/')
    else:
        form = LeadForm()
    return render(request, 'cwjleadsapp/submit_lead.html', {'form': form})

def card_lead(request):
    if request.method == 'POST':
        form = CardForm(request.POST, request.FILES)
        
        if form.is_valid():
            # any processing
            form.save()
            return HttpResponseRedirect('/')
    else:
        form = CardForm()
    return render(request, 'cwjleadsapp/submit_card.html', {'form': form})


class LeadUpdate(LoginRequiredMixin, UpdateView):
    model = Lead
    template_name_suffix = '_update_form'
    fields = ['name', 
            'work_email', 
            'phone_number', 
            'zip_code', 
            'office_size', 
            'message',
            'business_card']

class LeadDelete(LoginRequiredMixin, DeleteView):
    model = Lead
    success_url = reverse_lazy('index')

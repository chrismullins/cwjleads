from django.contrib import admin

# Register your models here.
from .models import Lead

@admin.register(Lead)
class LeadInstanceAdmin(admin.ModelAdmin):
    list_display = ('work_email', 'zip_code', 'office_size')

from django.db import models
from django.core.validators import RegexValidator, MinValueValidator, EmailValidator
from django.urls import reverse

# Create your models here.
class Lead(models.Model):
    """
    Model representing a lead
    """
    name  = models.CharField(max_length=200, 
        help_text="Enter your first and last name",
        blank=True)
    email_validator = EmailValidator()
    work_email = models.CharField(max_length=200, 
         help_text="Enter your work email",
         validators=[email_validator],
         blank=True)
    
    # Check this for form validation: https://stackoverflow.com/questions/19130942/whats-the-best-way-to-store-phone-number-in-django-models
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    phone_number = models.CharField(validators=[phone_regex], 
        max_length=17, 
        help_text="Enter your phone number", 
        blank=True) # validators should be a list

    zip_code = models.CharField(max_length=5, 
        help_text="Enter your office zip code",
        blank=True)
    office_size = models.PositiveIntegerField(validators=[MinValueValidator(1)], 
        help_text="Enter the number of people working in your office on a typical work day.",
        blank=True, null=True)
    message = models.TextField(max_length=1000, 
        help_text="Enter a brief message about your needs.",
        blank=True)
    business_card = models.ImageField(upload_to='cards/',
        null=True, 
        blank=True)
    def __str__(self):
        return self.work_email + '(' + self.zip_code + ')'

    def get_absolute_url(self):
        return reverse('lead-detail', args=[str(self.id)])
